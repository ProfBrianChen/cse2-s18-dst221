//Delton Tschida,CSE2,Prof.Chen,3/20/18
//This program creats an Argyle

import java.util.Scanner;  //import scanner
public class Argyle{ 	  
public static void main(String [] args) { //main method
Scanner scanner = new Scanner(System.in); // make a scanner
    int count = 0;				//make counter to advance through loops
    int width = 0;				//width
    int height = 0;				//height
    int d_width = 0;			//diamond width
    int stripe = 0;				//stripe width
    String check = " ";  //check string entered by user
    String d_pattern = " "; //store diamond pattern
    String s_pattern = " "; //store stripe pattern
    String f_pattern = " ";	//store fill pattern
    int store=0;//store a number for comparison
    while (count == 0){  
      System.out.println("Please enter a positive integer for the width of the window: "); //prompts for user input
      if(scanner.hasNextInt()){
     store=scanner.nextInt();}
      if(store>0){
      count=count+1;
        width=store;
        scanner.nextLine();		// clear the scanner for new storage
      }
      else{
        count=0;
        scanner.nextLine();		// clear
      }
     
      
      
    } //window width
      while (count == 1){  
      System.out.println("Please enter a positive integer for the height of the window: "); //prompts for user input
      if(scanner.hasNextInt()){
     store=scanner.nextInt();}
      if(store>0){
      count=count+1;
        height=store;
        scanner.nextLine();		// clear
      }
      else{
        count=1;
        scanner.nextLine();		// clear
      }
     
      
      
    }
    while (count == 2){
      System.out.println("Enter a positive integer for width of argyle "); //ask user for input
      if (scanner.hasNextInt()){			//check input
        store= scanner.nextInt();
        if(store>0){
          d_width=store;
        }
       count=count+1;
        
      }
      scanner.nextLine();		// clears scanner
    } //diamond width
  
  
    while (count == 3){
      System.out.println("enter a positive odd integer for argyle stripe width "); //prompts for user input
      if (scanner.hasNextInt()){			//checks input is an integer
         store = scanner.nextInt();
        if((store%2==1)&&store>0){
         count=count+1;
         store=stripe;
       } 	
       
      }
      scanner.nextLine();		// clears scanner
    } //stripe width
    while (count == 4){
      System.out.println("Enter a character for the backround pattern ");  //propts user for the background character
      check = scanner.next();     //assigns next string to designated variable
      if ( check.length() == 1){  //ensures string length is equal to 1
        f_pattern = check;      
        count = count+1;                      //increases count
      }
      scanner.nextLine();     //clears scanner
    } //background pattern
    while (count == 5){
      System.out.println("Enter a character to be used as the diamond pattern ");  //propts user for the diamond character
      check = scanner.next();     //assigns next string to designated variable
      if ( check.length() == 1){  //ensures string length is equal to 1
        d_pattern = check;      
        count = count+1;                      //increases count
      }
      scanner.nextLine();     //clears scanner
    } //diamond pattern  
    while (count == 6){
      System.out.println("Enter a character to be used as the stripe");  // get stripe character
      check = scanner.next();     //assigns next string to variable
      if ( check.length() == 1){  //ensures string length is equal to 1
        s_pattern = check;      
        count = count+1;                      //increases count
      }
      scanner.nextLine();     //clear scanner
    } //stripe pattern
    
  //begin constructing the argyle 

}
}