//Delton Tschida,CSE2,Prof.Chen,2/20/18
//This program calculates the score of a yahtzee game

import java.util.Random;//needed for random number generation
import java.util.Scanner;//needed for user inputs
public class Yahtzee{//main method for java program
public static void main(String[] args) {//needed before start of program
//Create ints to hold the values of the five dice
 int die1=0;
 int die2=0;
 int die3=0;
 int die4=0;
 int die5=0;
  
Scanner myScanner = new Scanner(System.in);//Needed to accept user input
System.out.print("Enter 1 to select numbers or enter 2 to have computer roll: ");//have user choose whether they want to choose roll or fo random
double choice = myScanner.nextDouble();//Store user choice
int run=0; //Varible that keeps track of whether the user entered valid numbers or not
  if (choice !=1 && choice != 2) {//Display error if user dosn't select valid option
      System.out.println("Select a valid option, 1 or 2");
    }

   if (choice == 1) {

      System.out.print("enter 5 numbers with no spaces: ");//tell user to enter their chosen numbers and then store them as an int
      int number = myScanner.nextInt();
      
      int length = String.valueOf(number).length(); //checks the length
     run=run+1;//because choice is valid add 1 so program will run
      if (length !=5){//check to make sure 5 numbers were entered
        System.out.println("Error, enter a 5 digit number");
        run=run-1;//length is not correct, subtratct 1 so program will not run
      }
     //use math convert 5 digit number to five dice values
      die1 = number % 10;
      die2 = number/10 % 10;
      die3 = number/100 % 10;
      die4= number/1000 % 10;
      die5 = number/10000 % 10;
     
if(die1>6||die2>6||die3>6||die4>6||die5>6){// make sure all values are less than 6
System.out.println("you entered an invalid number");
  run=run-1;//A value is out of range,subtract 1 to not let program run
}
   }
  if(choice==2){
 //get the random values of all six dice
die1 = (int)(Math.random()*6)+1;
die2 = (int)(Math.random()*6)+1;
die3 = (int)(Math.random()*6)+1;
die4 = (int)(Math.random()*6)+1;
die5 = (int)(Math.random()*6)+1;
run=run+1;//Choice is valid add 1 so program will run
  }
 if(run>0){//run program if valid user input was entered previously


//create ints to hold the number of times a value appears on the dice
int num1=0;
int num2=0;
int num3=0;
int num4=0;
int num5=0;
int num6=0;
   
//The following switch statements calculate how many times a number appears accross all the dice
switch(die1){
  case 1: die1=1;
    num1++;
  break;
  case 2: die1=2;
    num2++;
  break;
  case 3: die1=3;
    num3++;
  break;
  case 4: die1=4;
    num4++;
  break;
  case 5: die1=5;
    num5++;
  break;
  case 6: die1=6;
    num6++;
  break;
    }
  
  switch(die2){
  case 1: die2=1;
    num1++;
  break;
  case 2: die2=2;
    num2++;
  break;
  case 3: die2=3;
    num3++;
  break;
  case 4: die2=4;
    num4++;
  break;
  case 5: die2=5;
    num5++;
  break;
  case 6: die2=6;
    num6++;
  break;
    }
  
  switch(die3){
  case 1: die3=1;
    num1++;
  break;
  case 2: die3=2;
    num2++;
  break;
  case 3: die3=3;
    num3++;
  break;
  case 4: die3=4;
    num4++;
  break;
  case 5: die3=5;
    num5++;
  break;
  case 6: die3=6;
    num6++;
  break;
    }
  
  switch(die4){
  case 1: die4=1;
    num1++;
  break;
  case 2: die4=2;
    num2++;
  break;
  case 3: die4=3;
    num3++;
  break;
  case 4: die4=4;
    num4++;
  break;
  case 5: die4=5;
    num5++;
  break;
  case 6: die4=6;
    num6++;
  break;
    }
  
  switch(die5){
  case 1: die5=1;
    num1++;
  break;
  case 2: die5=2;
    num2++;
  break;
  case 3: die5=3;
    num3++;
  break;
  case 4: die5=4;
    num4++;
  break;
  case 5: die5=5;
    num5++;
  break;
  case 6: die5=6;
    num6++;
  break;
    }
   
  //Create ints to hold the totals for the sections
  int lower_total=0;
  int upper_total=0;
  int grand_total=0;
  
  if(num1==5||num2==5||num3==5||num4==5||num5==5){//checks for Yahtzee
  lower_total=lower_total+50;
  }
  if(num1==1&&num2==1&&num3==1&&num4==1&&num5==1){//checks for large straight
    lower_total=lower_total+40;
  }
 if ((die2 == die1 + 1) && ((die3 == die2 + 1) && (die4 == die3 + 1))){ //checks for smallStraight
      lower_total=lower_total+30;
    }
if((num1==3||num2==3||num3==3||num4==3||num5==3)&&(num1==2||num2==2||num3==2||num4==2||num5==2)){//checks for full house
  lower_total=lower_total+25;
}
if(num1>=4||num2>=4||num3>=4||num4>=4||num5>=4){//checks for 4 of a kind
lower_total=lower_total+(die3*4);
}
if(num1>=3||num2>=3||num3>=3||num4>=3||num5>=3){//checks for 3 of a kind
lower_total=lower_total+(die3*3);
}
lower_total=lower_total+(die1+die2+die3+die4+die5);//add chance box to score
//Calculate upper section
 if(num1>=3){
 upper_total=upper_total+(num1*1);
 }
 if(num2>=3){
 upper_total=upper_total+(num2*2);
 }
 if(num3>=3){
 upper_total=upper_total+(num3*3);
 }
 if(num4>=3){
 upper_total=upper_total+(num4*4);
 }
 if(num5>=3){
 upper_total=upper_total+(num5*5);
 }
 if(num6>=3){
 upper_total=upper_total+(num6*6);
 }
int upper_bonus=0;
if(upper_total>=63){//add bonus if score is high enough
upper_bonus=upper_total+35;
}
  else{
 upper_bonus=upper_total;
  }
 
grand_total=upper_total+lower_total; //add lower and upper sections for grand totals
  

System.out.println("The intial total of the upper section is: "+upper_total);
System.out.println("The upper total with bonus is: "+upper_bonus);
System.out.println("The lower total is: "+lower_total);
System.out.println("The grand total is: "+grand_total);
}
}
}