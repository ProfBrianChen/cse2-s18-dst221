//Delton Tschida,2/9/18,CSE2,Prof.Chen
//This program calculates how to split a bill with tip.
import java.util.Scanner;

public class Check{
    			// main method required for every Java program
   			public static void main(String[] args) {
Scanner myScanner = new Scanner( System.in );//make a scanner
System.out.print("Enter the original cost of the check in the form xx.xx: ");//prompt user to enter check total

double checkCost = myScanner.nextDouble();// Store check cost as a double

System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx):");// Promot user to enter the percentage tip they want
double tipPercent = myScanner.nextDouble();//Get user tip amount and store as a double
tipPercent /= 100; //We want to convert the percentage into a decimal value
System.out.print("Enter the number of people who went out to dinner: ");//Prompt user for number of ways to split check
int numPeople = myScanner.nextInt();//Store number of people as an int

double totalCost;//create double to store total cost
double costPerPerson;//create double to store the cost per person
int dollars,   //whole dollar amount of cost 
      dimes, pennies; //for storing digits
          //to the right of the decimal point 
          //for the cost$ 
totalCost = checkCost * (1 + tipPercent);//Calculate total cost
costPerPerson = totalCost / numPeople;//Calculate cost per person
//get the whole amount, dropping decimal fraction
dollars=(int)costPerPerson;
//get dimes amount, e.g., 
// (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
//  where the % (mod) operator returns the remainder
//  after the division:   583%100 -> 83, 27%5 -> 2 
dimes=(int)(costPerPerson * 10) % 10;
pennies=(int)(costPerPerson * 100) % 10;
System.out.println("Each person in the group owes $" + dollars +'.'+ dimes + pennies );//Show the result


}  //end of main method   
  	} //end of class