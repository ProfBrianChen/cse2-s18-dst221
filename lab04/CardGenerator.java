//Delton Tschida,CSE2,Prof.Chen,2/11/18
//This program select a random card from a standard deck

import java.util.Random;//needed to generate random number
public class CardGenerator{//main method for java program
public static void main(String[] args) {//needed before start of program
int number = (int)(Math.random()*52)+1; //get random number
System.out.println("the number is: "+ number);//show user number
String suit;//make string to store suit
//Based on number assign it to a suit
if(number<=13){
suit="Diamonds";
}
  else if(number>=14 && number<=26){
    suit="Clubs";
  }
      else if(number>=27 && number<=39){
        suit="Hearts";
      }
          else{
            suit="Spades";
          }
  int kind=number%13;//get remainder to begin assigning values to suits
  String face="random";//Make a string to store face value
  int num_face=0;//Make a string to store number on face
//assign cards their face value
if(kind==1){
   face="1";
 }
  else if(kind==11){
   face="jack";
  }
  else if(kind==12){
    face="queen";
  }
  else if(kind==13){
    face="King";
  }
  else{
    num_face=kind;
  }
  
  //Based on the face value show the user the suit and face value
  if(kind==1||kind==11||kind==12||kind==13){
  System.out.println("The Card is the "+face+" of "+suit);
  }
  else{
   System.out.println("The Card is the "+num_face+" of "+suit);  
  }
    
    
  
  
  
  
  
  
  
}//end of main method
}//end of class