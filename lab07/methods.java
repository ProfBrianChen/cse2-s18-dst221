//Delton Tschida,CSE2,Prof.Chen,3/6/18
//This program generates a random sentance

import java.util.Scanner;//needed for user inputs
import java.util.Random;//needed for random number generation
public class methods{//main method for java program

public static String create_adj(int x){//this method randomly selects and return an adjective
Random randomGenerator = new Random();
int randomInt = randomGenerator.nextInt(10);
String adj=" ";
switch(randomInt){
  case 0:
    adj="fast";
    break;
  case 1:
    adj="smart";
    break;
  case 2:
    adj="slow";
    break;
   case 3:
    adj="silly";
    break;
  case 4:
    adj="best";
    break;
  case 5:
    adj="tough";
    break;
  case 6:
    adj="strong";
    break;
  case 7:
    adj="weak";
    break;
  case 8:
    adj="worst";
    break;
  case 9:
    adj="ridiculous";
    break;
    
}
  return adj;  
}
  
  
  public static String create_noun(int x){//thsi method randomly selects a noun
Random randomGenerator = new Random();
int randomInt = randomGenerator.nextInt(10);
String adj=" ";
switch(randomInt){
  case 0:
    adj="fox";
    break;
  case 1:
    adj="dog";
    break;
  case 2:
    adj="bear";
    break;
   case 3:
    adj="wolf";
    break;
  case 4:
    adj="man";
    break;
  case 5:
    adj="woman";
    break;
  case 6:
    adj="elephant";
    break;
  case 7:
    adj="horse";
    break;
  case 8:
    adj="house fly";
    break;
  case 9:
    adj="cat";
    break;
    
}
  return adj;  
}
  
  
  
public static String create_verb(int x){//thsi method randomly selects a verb
Random randomGenerator = new Random();
int randomInt = randomGenerator.nextInt(10);
String adj=" ";
switch(randomInt){
  case 0:
    adj="passed";
    break;
  case 1:
    adj="punched";
    break;
  case 2:
    adj="swallowed";
    break;
   case 3:
    adj="ate";
    break;
  case 4:
    adj="killed";
    break;
  case 5:
    adj="kicked";
    break;
  case 6:
    adj="saved";
    break;
  case 7:
    adj="watched";
    break;
  case 8:
    adj="beat";
    break;
  case 9:
    adj="threw";
    break;
    
}
  return adj;  
}

public static String create_objectnoun(int x){//this methos randomly selects a object noun
Random randomGenerator = new Random();
int randomInt = randomGenerator.nextInt(10);
String adj=" ";
switch(randomInt){
  case 0:
    adj="grandma";
    break;
  case 1:
    adj="grandpa";
    break;
  case 2:
    adj="mom";
    break;
   case 3:
    adj="dad";
    break;
  case 4:
    adj="professor";
    break;
  case 5:
    adj="teacher";
    break;
  case 6:
    adj="student";
    break;
  case 7:
    adj="toddler";
    break;
  case 8:
    adj="baby";
    break;
  case 9:
    adj="uncle";
    break;
    
}
  return adj;  
}

  
public static String thesis(int x){//creats the first sentence and choose subject
 String subject=create_noun(x);
 
 System.out.println("The " + create_adj(x)+" " + subject +" " + create_verb(x) + " the " + create_adj(x) +" "+ create_objectnoun(x)); 
 
 return subject;
}

public static String action(String subject,int x){//randomly creates a random number of action sentences

 for(int i=0;i<x+1;i++){
 System.out.println("The " + create_adj(x)+" " + subject +" " + create_verb(x) + " the " + create_adj(x) +" "+ create_objectnoun(x));
 System.out.println("After, The " + subject +" " + create_verb(x) + " the " + create_adj(x) +" "+ create_objectnoun(x)); 
 }
 return " ";
}

public static String conclusion(String subject,int x){// writes a random  conclusion sentence
 System.out.println("In conclusion,the  " + create_adj(x)+" " + subject +" " + create_verb(x) + " the " + create_adj(x) +" "+ create_objectnoun(x)+" many times"); 
 return "";
}
  
  
public static String end (int x){// calls all methods to write a random story
String subject=" ";

subject=thesis(x);
Random randomGenerator = new Random();
x = randomGenerator.nextInt(4);
action(subject,x);
conclusion(subject,x);


return " ";
}

public static void main(String[] args) {//needed before start of program
String adj1=" ";
int loop=0;
int x=1;
String subject="";

Scanner myscanner = new Scanner(System.in); // make a scanner
while(loop==0){//makes random stories untill user exits
System.out.println(end(x));
System.out.println("Enter 0 to get another story or 1 to end");
loop=myscanner.nextInt();// store string

}

}
}
