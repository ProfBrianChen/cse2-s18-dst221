//Delton Tschida
//la09 4/17/18
// This code manipulates an array

import java.util.Scanner;

public class pass{
  
  public static int[] copy(int[] array){
    int[] array2 = new int[array.length];
    for (int i = 0; i < array.length; i++){
      int val = array[i];
      array2[i] = val;
    }
    return array2;
  }
  
  public static void print(int[] array){
    for (int i = 0; i < array.length; i++){
      System.out.print(array[i]+" ");
    }
    System.out.println(" ");  
  }
  
  
  
  public static void inverter(int[] array){//this method switches array elements to reverse the order
    for (int i = 0; i < array.length/2; i++){
      int index = array[i];
      array[i] = array[array.length - i - 1];
      array[array.length - i - 1] = index;
    }
  }
  
  public static int[] inverter2(int[] array){
    int[] list = copy(array);
    for (int i = 0; i < list.length/2; i++){
      int val = list[i];
      list[i] = list[list.length - i - 1];
      list[list.length - i - 1] = val;
    }
    return list;
  }
  
  public static void main(String[] args){
    int[] array0 = {1, 2, 3, 4, 5, 6, 7, 8, 9};//make array
    int[] array1 = copy(array0);//make copies 
    int[] array2 = copy(array0);
    inverter(array0);// call method to invert array
    System.out.println("Array0:");
    print(array0);//print
    inverter2(array1);//invert
    System.out.println("Array1:");
    print(array1);
    int[] array3 = inverter2(array2);
    System.out.println("Array3:");
    print(array3);
  }
}
  