//Delton Tschida,CSE2,Prof.Chen,3/6/18
//This program Generates a twist pattern

import java.util.Scanner;//needed for user inputs
public class TwistGenerator{//main method for java program
public static void main(String[] args) {//needed before start of program
Scanner myScanner = new Scanner(System.in);//Needed to accept user input
System.out.println("Enter a positive integer for the lenght of the twist");//prompt user for a number
int length;//varible to store twist lenght
length=0;//set intial lenght to zero
boolean run=true;//flag to keep track of when a loop should run
 while(run){//loop that runs until there is an apropiate input from the user
   if (myScanner.hasNextInt()){
     length=myScanner.nextInt();//if an int is entered, store it as lenght
     run=false;//set run to false so it does not run again
   }
   else{
     System.out.print("Only a positive integer if acceptable");//if a positive int is not entered display this message to user
     myScanner.next();// clear what is stored
   }
 }
  if(length<0){//check to make sure int is positive
    System.out.println("Please enter a positive integer");
  }
  //The following for loops will construct the twist
  int i=0;//int to store row one of twist
  int j=0;//int to store row two of twist
  int k=0;//int to store row 3 of twist
  for(i=0;i<length;i++){// this loop prints top row
    if(i%3==0){//the pattern has 3 repeating options, get remainder to decide what to do 
      System.out.print("\\");
    }
    else if(i%3==1){
      System.out.print(" ");
      
    }
    else{
      System.out.print("/");
    }
    }
  System.out.println();//move down a line
    for(j=0;j<length;j++){//same as previous for loop but will print second row
    if(j%3==0){
      System.out.print(" ");
    }
    else if(j%3==1){
      System.out.print("X");
      
    }
    else{
      System.out.print(" ");
    }
    }
  System.out.println();//move down line
  
    for(k=0;k<length;k++){//same as previous for loops but prints 3rd line
    if(k%3==0){
      System.out.print("/");
    }
    else if(k%3==1){
      System.out.print(" ");
      
    }
    else{
      System.out.print("\\");
    }
    }
  

}
}
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
 
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  

  