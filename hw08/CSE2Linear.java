//Delton Tschida,CSE2,Prof.Chen,4/10/18
//This program makes an array of ints and looks through it for specific values

import java.util.Scanner;  //import scanner
import java.util.Arrays;//import arrays
import java.util.Random;//needed for random number generation
public class CSE2Linear{//class
public static void binarySearch( int[] grades, int s )//method for binary search
 {
  int k=0;//counts iterations
    int low = 0;//bottom of index
    int high = grades.length-1;  //top of index
    while( low<=high )
    {
       int mid = low+(high-low)/2;//middle of index
       if( s<grades[mid] ){
          high = mid-1;
      k++;
       }
       else if( s>grades[mid] ){
          low = mid+1;
      k++;
    }
       else{
         System.out.println("The grade was found and the number of iterations used in the binary search was "+k);
          return;
       }
    }
  System.out.println("The grade was not found and the number of iterations used in the binary search was "+k);
  return;
 }
public static void linearSearch( int[] grades, int s ){//linear search method
 int  k=0;// counts iterations
    for(int i=0;i<grades.length;i++){//look through index untill values is found
      k++;
            if(grades[i] == s){    
           System.out.println("The grade was found and the number of iterations used in the linear search was "+k);
          return;    
            }    
        } 
  System.out.println("The grade was not found and the number of iterations used in the linear search was "+k);
        return;    
    
  
}  
  
public static void main(String [] args) { //main method
Scanner myscanner = new Scanner(System.in); // make a scanner
int[] grades;//array to store grades
grades=new int[15];//declare size
int grade=0;//intial  value 
int previous_grade=0;//used to store previous grade entered
boolean run=false;//value to determine whether to run loop or not
for(int i=0;i<15;i++){// for loop to take all 15 grades
  System.out.println("Enter the student "+ (i+1) + "'s final grade");
  if(myscanner.hasNextInt()){
    grade=myscanner.nextInt();// if an int is entered store as grade value
    run=true;// set later loop to run so int grade value can be checked further
  }
  else{
  System.out.println("you may only enter integers");
      i--;  
   }
 myscanner.nextLine();//clear scanner
  while(run){
    if (grade<previous_grade){//maek sure grade is greter than last one  entered
      System.out.println("The grade must be greater than the previous grade entered");
      run=false;
      i--;
    }
   else if(grade>=0&&grade<=100){//make sure range is correct
   grades[i]=grade;// store grade value in array
   previous_grade=grade;// set current grade to prevoius array for the new grade to be compared to 
   run=false;
  }
    else if(grade>100||grade<0) {//check grade range
      System.out.println("The grade must be between 0 and 100");
     run=false;
      i--;
    }
    else{
      System.out.print(" ");
      run=false;
    }
  }
  run=false;// set run back to false
  }
  
for(int x=0;x<15;x++){// print array
  System.out.print(grades[x]+" ");
}
 System.out.println("Enter a value to search for");// get value to search for
 int s=myscanner.nextInt();
binarySearch(grades,s);//call binary search method
  
for (int i=0; i<grades.length; i++) {// this loop scrambles the array
	int target = (int) (grades.length * Math.random());
	//swap the values
	int temp = grades[target];
	grades[target] = grades[i];
	grades[i] = temp;
}
System.out.println("The array has been scrambled, the new array is: ");// display new array
for(int j=0;j<15;j++){
  System.out.print(grades[j]+" ");
}
System.out.println("Enter a another value to search for");// get value for linear search
 s=myscanner.nextInt();
linearSearch(grades,s);// run linear search method

  
}
}