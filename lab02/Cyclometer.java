// Delton Tschida,CSE2,Prof. Chen, 2/8/18
//
public class Cyclometer {
// main method required for every Java program
   	public static void main(String[] args) {
	 	int secsTrip1=480;  // Time in Seconds First Trip
   	int secsTrip2=3220;  // Time in Seconds Second Trip
		int countsTrip1=1561;  // count trip 1
		int countsTrip2=9037; // count trip 2
      
    double wheelDiameter=27.0,  //Diameter of wheel 
  	PI=3.14159, // Pi constant
  	feetPerMile=5280,  // Feet in a mile
  	inchesPerFoot=12,   // Inches in a foot
  	secondsPerMinute=60;  // Seconds in a minute
	double distanceTrip1, distanceTrip2,totalDistance;  // Doubles for storing distance 1,2,and total distance
   
    System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");// Print out time of trip and wheel count
	       System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");// Print out time of trip 2 and wheel  count
      
  //Calculate the distance travled on trips 1&2
	distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;// Calculate distance of trip 2
	totalDistance=distanceTrip1+distanceTrip2; //Calculatge total distance
      
    //Print out the output data.
  System.out.println("Trip 1 was "+distanceTrip1+" miles");// print out distance of trip 1
	System.out.println("Trip 2 was "+distanceTrip2+" miles");//pritn out distance of trip 2
	System.out.println("The total distance was "+totalDistance+" miles");// print out the total distance


	}  //end of main method   
} //end of class