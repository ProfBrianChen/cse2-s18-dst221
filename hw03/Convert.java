//Delton Tschida,CSE2,Prof.Chen,2/11/18
//This program converts the amount  
//of rain dropped by a hurrican into cubic miles.

import java.util.Scanner;//needed for user inputs
public class Convert{//main method for java program
public static void main(String[] args) {//need before start of program
Scanner myScanner = new Scanner( System.in );//make a scanner
System.out.print("How many acres of land were affected by the hurricane's rain? ");// Get number of acres from user
double acres = myScanner.nextDouble();// Store acres as a double
System.out.print("Enter the rainfall in inches: ");//get inches from user
double inches = myScanner.nextDouble(); // Store inches as a double
double sqaure_mile=acres*0.0015625;//convert acres to square miles
double mile_inches=inches*0.000015783;//convert inches to miles
double cubic_miles= sqaure_mile*mile_inches;//calculate cubic miles
System.out.print("The cubic miles of rain dropped is:"+cubic_miles);//display the result
}  //end of main method   
  	} //end of class