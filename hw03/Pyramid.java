//Delton Tschida,CSE2,Prof.Chen,2/11/18
//This program calculates the volume inside of a pyramid.

import java.util.Scanner;//needed for user inputs
public class Pyramid{//main method for java program
public static void main(String[] args) {//needed before start of program
Scanner myScanner = new Scanner( System.in );//make a scanner
System.out.print("Enter the square side length of the pyramid: ");// Get the side length of the  pyramid from the user
double length = myScanner.nextDouble();// Store length as a double
System.out.print("Enter the height of the pyramid");//get the height from the user
double height=myScanner.nextDouble();//Store height as a double
double volume=(length*length)*(height/3);// Calculate the volume of the pyramid
System.out.print("The volume inside the pyramid is: "+ volume);// display the result to the user
}//end of main method
}//end of class