public class Arithmetic {
public static void main(String[] args){
//Number of pairs of pants
int numPants = 3;
//Cost per pair of pants
double pantsPrice = 34.98;

//Number of sweatshirts
int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;

//Number of belts
int numBelts = 1;
//cost per belt
double beltCost = 33.99;

//the tax rate
double paSalesTax = 0.06;
//total pants price
double PantsPrice= numPants*pantsPrice;
//total shirt price
double ShirtPrice=numShirts*shirtPrice;
// total belt price
double BeltPrice=numBelts*beltCost;
//tax on belt
double tax_belt=BeltPrice*paSalesTax;
// tax on shirt
double tax_shirt=ShirtPrice*paSalesTax;
 //tax on pants
double tax_pants=PantsPrice*paSalesTax;
// total cost without tax
double Cost_no_tax=PantsPrice+ShirtPrice+BeltPrice;
//total tax
double total_tax= tax_pants+tax_shirt+tax_belt;
//total price paid
double total= Cost_no_tax+total_tax;


System.out.println("Cost of pants: " + (int)(PantsPrice*100)/100.0);
System.out.println("Cost of belts: "+(int)(BeltPrice*100)/100.0);
System.out.println("Cost of shirts: " + (int)(ShirtPrice*100)/100.0);
System.out.println("Tax on pants: "+ (int)(tax_pants*100)/100.0);
System.out.println("Tax on belts: "+(int)(tax_belt*100)/100.0);
System.out.println("Tax on shirts: " + (int)(tax_shirt*100)/100.0);
System.out.println("Price before tax: "+(int)(Cost_no_tax*100)/100.0);
System.out.println("Total Tax: "+ (int)(total_tax*100)/100.0);
System.out.println("Total Cost: "+(int)(total*100)/100.0);
  
  
}
} 