//Delton Tschida,CSE2,Prof.Chen,4/17/18
//This program plays a poker game

import java.util.Scanner;  //import scanner
import java.util.Arrays;//import arrays
import java.util.Random;//needed for random number generation
public class DrawPoker{//class
  
  
public static Boolean pair(int[] hand_check){//this method checks for a pair
 int num1=hand_check[0];
 int num2=hand_check[1];
 int num3=hand_check[2];
 int num4=hand_check[3];
 int num5=hand_check[4];
 if(num1/13==num2/13&&num3/13==num4/13&&num3/13==num5/13){//check for a flush
   return true;
 }
if(num1%13==num2%13&&num2%13==num3%13||num1%13==num2%13&&num2%13==num4%13||(num1%13)==(num2%13)&&num2%13==num5%13||(num2%13)==(num3%13)&&num3%13==(num4%13)||(num3%13)==(num4%13)&&num3%13==num5%13){//check for three of a kind
  return true;
}
return false;

 
}
  
  
public static int[] get_deck(){//this method generates a shuffled deck
int deck1[]=new int[52];
for(int i=0;i<=51;i++){
 deck1[i]=i;
}
for (int j=0; j<=51; j++) {// this loop scrambles the array
	int target = (int) (51 * Math.random());
	//swap the values
	int temp = deck1[target];
	deck1[target] = deck1[j];
	deck1[j] = temp;
}
return deck1;// return shuffled deck
}  
  
  
  
  
  
  
  
  

public static String card(int number){//this method returns card type
String suit=" ";
String face=" ";
int suit1=number/13;//calculate suit 
int face1=number%13;//calculate face value

if(suit1<1){// assign suit to card 
  suit="diamonds";
}
else if(suit1>=1 && suit1<2){
  suit="clubs";
}
else if (suit1>=2 && suit1<3){
  suit="hearts";
}
else{
  suit="spades";
}
  
switch(face1){// assign face value to card
  case 0:
    face="Ace";
    break;
 case 1:
    face="1";
    break;
 case 2:
    face="2";
    break;
 case 3:
    face="3";
    break;
 case 4:
    face="4";
    break;
 case 5:
    face="5";
    break;
 case 6:
    face="6";
    break;
 case 7:
    face="7";
    break;
 case 8:
    face="8";
    break;
 case 9:
    face="9";
    break;
 case 10:
    face="Jack";
    break;
 case 11:
    face="Queen";
    break;
 case 12:
    face="King";
    break;
  default:
    System.out.println("Error face type");
} 
String card_type= face +" of "+ suit;// store the card type
return card_type;// return the card type
}
  

  
  
  
  
public static void main(String [] args) { //main method
Scanner myscanner = new Scanner(System.in); // make a scanner
  
int deck[]=new int[52];//declare the deck of cards
deck=get_deck();// This calls a method that returns a shuffled deck of cards

System.out.println("The cards will now be delt");
String cardtype=" "; //string to store card type
for(int k=0;k<10;k++){
int number=deck[k];
cardtype= card( number );
if (k==0||(k%2==0)){//display the cards player 1 was delt
System.out.println("Player one was delt the "+cardtype);
}
else{//display cards player 2 was delt
  System.out.println("Player two was delt the "+cardtype);
}
}
Boolean hand1pair=false;//store if a pair exists or not 
Boolean hand2pair=false;// store if a pair exists or not 
int hand1[]=new int[5];//stores the hand of player 1
int hand2[]=new int[5];//Stores the hand of player 2
for(int w=0;w<10;w=w+2){//create arraya for player 1's hand
  int r=0;
  hand1[r]=deck[w];
  r++;
}  
for(int e=1;e<10;e=e+2){//create array for player 2's hand
 int p=0;
 hand2[p]=deck[e];
 p++;
}  
hand1pair=pair(hand1);//call mehtod to check for pair
hand2pair=pair(hand2);//call method to check for pair 
if(hand1pair==true&&hand2pair==false){// display who wins if a pair id found
  System.out.println("Player 1 wins!");
}
  else if(hand2pair==true&&hand1pair==false){
    System.out.println("Player 2 wins!");
  }
  else{// display who wins in the case of high card 
    int high1=0;
    int high2=0;
    for(int h=0;h<5;h++){
      if(hand1[h]>high1){
        high1=hand1[h];
      }
       if(hand2[h]>high2){
        high2=hand2[h];
      }
      
    }
    if(high1>high2){
      System.out.println("Player 1 Wins!");
    }
    else{
      System.out.println("Player 2 Wins!");
    }
  }

  
}
}