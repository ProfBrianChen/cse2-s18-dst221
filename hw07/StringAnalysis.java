//Delton Tschida,CSE2,Prof.Chen,3/20/18
//This program checks if a string contains letters

import java.util.Scanner;  //import scanner
public class StringAnalysis{//class
  
public static Boolean check_string(String input){//method to check the whole string
char a='a';//used to check leters
char z='z';//used to check letters
int i=0;//counter
int length =input.length();//get string length
while(i<length){//run until all characters are checked
if(input.charAt(i)>=a&&input.charAt(i)<=z){
  //charater is letter, do nothing
}
else{
return false;//charater is not a letter return false
} 
i++;  //increment counter
}
return true;// all characters are letters return true
}
  
public static Boolean check_string(String input,int int_input){//method to check certain number of inputs
char a='a';//used for comparison
char z='z';//used for comparison
int i=0;//counter
int length =input.length();//get string length
if(length<int_input){// if user wants to check more than the string has, limit to checking only the number of characters in the string
int_input=length;
}
Boolean test=true;
while(i<int_input){//run untill the you check the number of characters the user specified
if(input.charAt(i)>=a&&input.charAt(i)<=z){
}
else{
return false;
} 
i++;  //increment counter
}
return true;
}
  
  
public static void main(String [] args) { //main method
Scanner myscanner = new Scanner(System.in); // make a scanner
Boolean check=true;//used to store whether or not a non letter was found
String input=" ";//ued to store user entered string
int int_input=0;// used to store number  of characters user wants to check
String s1="all";  // used to as comparison to see uf user entered all
int x=0;// stores which loop to run
String choice=" ";// stores users choice
  
System.out.println("Please enter a string to check");//promot user for string
input=myscanner.nextLine();// store string
while(x==0){//run untill input is correct
System.out.println("How many characters of the string would you like to check? Enter an integer or all"); // prompt user for input
if(myscanner.hasNextInt()){// run if int is entered
 int_input=myscanner.nextInt();
 x=2;// exit and go to loop 2
}
if(x==0){
choice=myscanner.nextLine();
if(choice.equals(s1)){// see if all was entered
x=4;
}
else{
System.out.println("error, enter an integer or all");// display error becuase no good input was entered
}
} 
} 
if(x==2){// call for method that checks string
check=check_string(input,int_input);
}
if(x==4){// call for method that checks string
check=check_string(input);
}
  
if(check==false){//display whether they are all lettere or not based on what the method returned
System.out.println("In the number of characters you wanted to check, one of them was not a letter");
}
else{
  System.out.println("In the number of characters you wanted to check, all of them were letters");
}

  
}
}