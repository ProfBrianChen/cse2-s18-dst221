//Delton Tschida,CSE2,Prof.Chen,3/20/18
//This program calculates the area of shapes

import java.util.Scanner;  //import scanner
public class Area{

public static double calculate_sqaure(double val1){//method to calculate sqaure area
double area= val1*val1;
return area;
}

public static double calculate_triangle(double val1,double val2){//method to calculate triangle area
double area= .5*val1*val2;
return area;
}
  
public static double calculate_circle(double val1){//method to calculate square area
double area= 3.14159*(val1*val1);
return area;
}
  
public static double check_input(int choice ,int run){//method to check all inputs
int x=0;//varible that allows the exiting of the main loop
double result=0;// the result to return back to main method
Scanner myScanner = new Scanner(System.in);// scanner
while(x==0){// run loop untill correct input is entered
  
  if(choice==3) {//choice 3 means user chose square
 System.out.println("enter the side length");
 if(myScanner.hasNextDouble()){
  result=myScanner.nextDouble();
  x=1;//exit loop
  return result;
}
else{
     System.out.println("enter a double");//tell user to enter a double
     myScanner.next();// clear what is stored
} 
}  
  
if(choice==2&&run==0) {//user chose triangle angle and run=0 means they have not entered any inputs yet
 System.out.println("enter the base lenght of the triangle");
 if(myScanner.hasNextDouble()){
  result=myScanner.nextDouble();
  x=1;
  return result;
}
else{
     System.out.println("enter a double");//if a positive int is not entered display this message to user
     myScanner.next();// clear what is stored
} 
}  
  
  
if(choice==2&&run==1) {// get second measurment of triangle
 System.out.println("enter the height of the triangle");
 if(myScanner.hasNextDouble()){
  result=myScanner.nextDouble();
  x=1;//exit loop
  return result;
}
else{
     System.out.println("enter a double");//if a positive int is not entered display this message to user
     myScanner.next();// clear what is stored
} 
}   
   
if(choice==1){
System.out.println("Enter the radius of the circle");// choice 1 means user chose cirlce
}
if(myScanner.hasNextDouble()){
  result=myScanner.nextDouble();
  x=1;//exit loop
  return result;
}
else{
     System.out.println("enter a double");//if a positive int is not entered display this message to user
     myScanner.next();// clear what is stored
}
}
return result;
}
  
  
  
  
  
public static void main(String [] args) { //main method
Scanner myscanner = new Scanner(System.in); // make a scanner
int run=0;//number of time user has successfullt entered a measurment
double area;//area
double val1=0; //measurment 1
double val2=0;//measurment 2
int choice=0;// used to store shape user has chose
String shape;// used to store string user enters for shape
int loop=1;// the current loop the program is running
while(loop==1){//this loop gets shape from user
System.out.println("Which shape would you like to calculate the area of? square circle or triangle?");
shape=myscanner.nextLine();  
String s1="circle";
String s2="triangle";
String s3="square";
//set the shape the user chose to int choice to be used later in program
if(shape.equals( s1 )){
loop= 2;
choice=1;
}
else if(shape.equals( s2 )){
loop= 3;
choice=2;
}
else if(shape.equals(s3)){
loop=4 ;
choice=3;
}
else{
  System.out.println("You must enter circle triangle or square");
  loop= 1;
}
}  
if(choice==1){//get input for square and check it 
val1=check_input(choice,run);
}
else if(choice==2){// get input for triangle and check it
val1=check_input(choice,run);
run=1;
val2=check_input(choice,run);
}
else if(choice==3){// get input for circle and check it
val1=check_input(choice,run);
}
else{
  System.out.println("error");
}
if(choice==1){// get are of circle 
area=calculate_circle(val1);
}
else if(choice==2){//get area of triangle 
area=calculate_triangle(val1,val2);
}
else{//get area of sqaure
  area=calculate_sqaure(val1);
}
System.out.println("the area is: "+ area);// display the area to user
}
}
